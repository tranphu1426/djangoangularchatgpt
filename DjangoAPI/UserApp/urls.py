from django.urls import re_path
from UserApp import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [

    re_path(r'^register/$', views.registerApi),
    re_path(r'^login$', views.loginApi),
]
