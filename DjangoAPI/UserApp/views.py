from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate
from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from UserApp.models import User
from UserApp.serializers import UserLoginSerializer, UserSerializer

import json


@csrf_exempt
def registerApi(request):
    user_data = JSONParser().parse(request)
    serializer = UserSerializer(data=user_data)
    print("serializer", serializer)
    if serializer.is_valid():
        serializer.validated_data['password'] = make_password(
            serializer.validated_data['password'])
        user = serializer.save()

        return JsonResponse({
            'message': 'Register successful!'
        }, status=status.HTTP_201_CREATED)

    else:
        return JsonResponse({
            'error_message': 'This email has already exist!',
            'errors_code': 400,
        }, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def loginApi(request):
    user_data = JSONParser().parse(request)
    serializer = UserLoginSerializer(data=user_data)
    print("is_valid: ", serializer.is_valid())
    if serializer.is_valid():
        user = authenticate(
            request,
            username=serializer.validated_data['email'],
            password=serializer.validated_data['password']
        )
        if user:
            refresh = TokenObtainPairSerializer.get_token(user)

            userModel = get_user_model()
            userInfo = userModel.objects.get(
                email=serializer.validated_data['email'])

            data = {
                'refresh_token': str(refresh),
                'access_token': str(refresh.access_token),
                'access_expires': int(settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME'].total_seconds()),
                'refresh_expires': int(settings.SIMPLE_JWT['REFRESH_TOKEN_LIFETIME'].total_seconds()),
                'id': userInfo.id,
                'email': userInfo.email,
                'firstName': userInfo.first_name,
                'lastName': userInfo.last_name,
                'password': userInfo.password,
            }            

            return JsonResponse(data, status=status.HTTP_200_OK)

        return JsonResponse({
            'error_message': 'Email or password is incorrect!',
            'error_code': 400
        }, status=status.HTTP_400_BAD_REQUEST)

    return JsonResponse({
        'error_messages': serializer.errors,
        'error_code': 400
    }, status=status.HTTP_400_BAD_REQUEST)
