from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from EmployeeApp.models import Departments, Employees
from EmployeeApp.serializers import DepartmentSerializer, EmployeeSerializer, QuestionSerializer

from django.core.files.storage import default_storage

import requests
import openai
import sseclient
import json

from django.http import HttpResponse, StreamingHttpResponse
from sseclient import SSEClient
from django.conf import settings
import os
import excel2img


openai.api_key = settings.STREAM_API_KEY

# define the path to the face detector
FACE_DETECTOR_PATH = os.path.join(os.path.dirname(__file__), 'media')

# Create your views here.
@csrf_exempt
def departmentApi(request, id=0):
    if request.method =='GET':
        departments = Departments.objects.all()
        department_serializer = DepartmentSerializer(departments, many=True)
        print("DATA TYPE \n", type(department_serializer.data))
        return JsonResponse(department_serializer.data, safe=False)
    elif request.method == 'POST':
        department_data = JSONParser().parse(request)
        department_serializer = DepartmentSerializer(data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse("Add Suscessfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    
    elif request.method == 'PUT':
        department_data = JSONParser().parse(request)
        department = Departments.objects.get(DepartmentId=department_data['DepartmentId'])
        department_serializer = DepartmentSerializer(department, data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse("Update Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)
    
    elif request.method == 'DELETE':
        department = Departments.objects.get(DepartmentId=id)
        department.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)
    


@csrf_exempt
def employeeApi(request, id=0):
    if request.method =='GET':
        employees = Employees.objects.all()
        employees_serializer = EmployeeSerializer(employees, many=True)
        print("DATA TYPE \n", type(employees_serializer.data))
        return JsonResponse(employees_serializer.data, safe=False)

    elif request.method == 'POST':
        employee_data = JSONParser().parse(request)
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse("Add Suscessfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    
    elif request.method == 'PUT':
        employee_data = JSONParser().parse(request)
        employee = Employees.objects.get(EmployeeId=employee_data['EmployeeId'])
        employee_serializer = EmployeeSerializer(employee, data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse("Update Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)
    
    elif request.method == 'DELETE':
        employee = Employees.objects.get(EmployeeId=id)
        employee.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)

@csrf_exempt
def SaveFile(request):
    file = request.FILES['uploadedFile']
    file_name = default_storage.save(file.name, file)

    return JsonResponse(file_name, safe=False)

@csrf_exempt
def ConvertExcel2img(request):
    file = request.FILES['uploadedFile']
    file_name_with_ext = default_storage.save(file.name, file)
    file_name = os.path.splitext(file_name_with_ext)[0]
    
    out_file_image = file_name + '.png'    

    source_directory = os.path.join(settings.MEDIA_ROOT, file_name_with_ext)
    output_directory = os.path.join(settings.OUTPUT_DIRECTORY, out_file_image)

    if os.path.exists(source_directory):
        excel2img.export_img(source_directory, output_directory)
    else:
        print("File not exist!!!")    

    return JsonResponse(out_file_image, safe=False)

@csrf_exempt
def ChatApi(request):

    if request.method == 'POST':

        question_data = JSONParser().parse(request)
        question_serializer = QuestionSerializer(data=question_data)
        if question_serializer.is_valid():

            prompt = question_serializer.data.get('QuestionText')            
            # completion = performRequestWithStreaming(prompt)
        
            completion = stream_realtime(prompt)
            return JsonResponse(completion, safe=False)
        
            # response = stream_json(prompt)
            # return JsonResponse(response, safe=False)
        
            # print(response)
            # messages = SSEClient(response)
            # return messages
            

            # response = openai.Completion.create(
            #     engine="text-davinci-003",
            #     prompt=prompt,
            #     max_tokens=1000,
            #     stream=True,
            #     temperature=0.7,
            # )

            # print("*****************************************************************************************")
            

            # def stream_response():
            #     for chunk in response:
            #         text = chunk.choices[0].text
            #         yield JsonResponse({'text': text})

            # return HttpResponse(stream_response(), content_type='application/json')

        return JsonResponse("Something went wrong. If this issue persists please contact us through our help center at help.openai.com.", safe=False)    


def performRequestWithStreaming(prompt):
    response = openai.Completion.create(engine="text-davinci-003", 
                                        prompt=prompt,
                                        max_tokens=1000,
                                        n=1,
                                        stop=None,
                                        temperature=0.5)
    return response.choices[0].text
    
def stream_realtime(prompt):
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=1000,
        stream=True,
        temperature=0.7,
    )
    print("*****************************************************************************************")
    result = ""
    for chunk in response:
        print(chunk.choices[0].text, end="", flush=True)
        # if '\n' in chunk:
        #     # Tách dữ liệu JSON từ chunk hiện tại
        #     json_str, extra = chunk.split('\n', 1)
        #     # Parse dữ liệu JSON và trả về kết quả
        #     result = result + extra.choices[0].text
        #     # Nếu còn dữ liệu thừa ở chunk hiện tại thì đưa nó vào chunk kế tiếp
        #     if extra:
        #         chunk = extra
        #     else:
        #         chunk = None
        # if chunk:
        #     result = result + chunk.choices[0].text
        
        result = result + chunk.choices[0].text
        
    return result



def stream_json(prompt):
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=1000,
        stream=True,
        temperature=0.7,
    )
    # for chunk in response:
    #     print(chunk.choices[0].text, end="", flush=True)
    return StreamingHttpResponse(stream_response_generator(response), content_type='text/event-stream')

def stream_response_generator(response):
    def stream_response():
        for chunk in response:
            text = chunk.choices[0].text
            yield JsonResponse({'text': text})

    return HttpResponse(stream_response(), content_type='application/json')
    # for chunk in response:
    #     response.stream(chunk)
    # chunks = response.choices[0].text.split('\n')
    # for chunk in chunks:
    #     yield chunk.choices[0].text        
