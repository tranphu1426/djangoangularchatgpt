# Django-basic-api


## Thiết lập môi trường 
```
python -m venv myenv
myenv\Scripts\activate
python -m pip install Django
pip install djangorestframework
```

## Vào project 
```
cd DjangoAPI
```

## Cài đặt middleware 'django-cors-headers':
```
pip install django-cors-headers
```


## Cài đặt middleware 'djangorestframework-simplejwt':
```
pip install djangorestframework-simplejwt
```

## Khời tạo và run app
```
py manage.py migrate
py manage.py runserver
```

## Trường hợp bị lỗi "django.db.utils.ProgrammingError: relation "company" already exists" thực thi câu lệnh sau rồi run lại app:
```
python manage.py migrate --fake
```
